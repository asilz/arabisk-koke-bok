loop:
    li t0 0
    bge a1, a0, jump0
    li t0 1
    add s0, a0, zero
    add a0, a1, zero
    add a1, s0, zero
    jump0:
    bge a2, a1, jump1
    li t0 1
    add s0, a1, zero
    add a1, a2, zero
    add a2, s0, zero
    jump1:
    bge a3, a2, jump2
    li t0 1
    add s0, a2, zero
    add a2, a3, zero
    add a3, s0, zero
    jump2:
    bge a4, a3, jump3
    li t0 1
    add s0, a3, zero
    add a3, a4, zero
    add a4, s0, zero
    jump3:
    bge a5, a4, jump4
    li t0 1
    add s0, a4, zero
    add a4, a5, zero
    add a5, s0, zero
    jump4:
    bne t0, zero, loop
    
    
